﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class Player_controller : MonoBehaviour{    

    [SerializeField]
    private float movementSpeed = 5.0f;
    public float jumpSpeed = 8f;   
    private float movement = 0f;
    private Rigidbody2D rigidBody;
    public Transform groundCheckPoint;
    public float groundCheckRadius;
    public LayerMask groundLayer;
    private bool isTouchingGround;
    public Vector3 respawnPoint;
    public GameManager gameManager;
    //public int coinValue = 1;

    private Animator animator;
    private string animationState = "AnimationState";
    enum AnimStates {
        Idle = 0,        
        WalkRight = 1,
        WalkLeft = 2,
        Crouch = 3,                      
    }
    // Start is called before the first frame update
    void Start() {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        respawnPoint = transform.position;
        gameManager = FindObjectOfType<GameManager>();
    }
    // Update is called once per frame
    void Update() {
        isTouchingGround = Physics2D.OverlapCircle(groundCheckPoint.position, groundCheckRadius, groundLayer);
        MovementHandler();
        AnimHandler();
        movement = Input.GetAxis("Horizontal");        
    }
    void OnCollisionStay() {
        //isGrounded = true;
    }
    private void MovementHandler() {        
        if (movement > 0f) {
            rigidBody.velocity = new Vector2(movement * movementSpeed, rigidBody.velocity.y);
        } else if (movement < 0f) {
            rigidBody.velocity = new Vector2(movement * movementSpeed, rigidBody.velocity.y);
        } else {
            rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
        }
        if (Input.GetButtonDown("Jump") && isTouchingGround) {
            SoundManagerScript.PlaySound("jump_06");
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpSpeed);
        }
    }

    private void AnimHandler() {
        if (movement > 0) {
            animator.SetInteger(animationState, (int)AnimStates.WalkRight);
        } else if (movement < 0) {
            animator.SetInteger(animationState, (int)AnimStates.WalkLeft);
        } else {
            animator.SetInteger(animationState, (int)AnimStates.Idle);
        }
        
    }

    public void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Ball") {
            Debug.Log("Collided with ball");
            //Destroy(collision.gameObject);
        }
        /*if (collision.gameObject.tag == "Coin") {
            SoundManagerScript.PlaySound("coin_07");
            gameManager.AddCoins(coinValue);
            Destroy(collision.gameObject);            
        }*/
        if (collision.gameObject.tag == "NewLevel") {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);                                        
        }
        if (collision.gameObject.tag == "Enemy") {
            SoundManagerScript.PlaySound("hit_11");
            Debug.Log("Enemy collision");
            transform.position = respawnPoint;
        }
        if (collision.gameObject.tag == "Bird") {
            SoundManagerScript.PlaySound("hit_09");
            Debug.Log("Bird collision");
            transform.position = respawnPoint;
        }
    }   

    public void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "FallDetector") {
            Debug.Log("Fall detector" );
            transform.position = respawnPoint;
        }
        if(other.tag == "Checkpoint") {
            Debug.Log("Checkpoint");
            respawnPoint = other.transform.position;
        }
        /*if (other.tag == "Bird") {
            Debug.Log("Hit bird");
            SoundManagerScript.PlaySound("hit_09");
            transform.position = respawnPoint;
        }*/
    }

}
