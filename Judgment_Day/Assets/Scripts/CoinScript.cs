﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CoinScript : MonoBehaviour
{

    private GameManager gameManager;
    public int coinValue = 1;
    public int score = 0;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player") {            
            SoundManagerScript.PlaySound("coin_07");            
            Destroy(gameObject);
            gameManager.AddCoins(coinValue);
        }                       
    }

}
