﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_script : MonoBehaviour{

    public float speed= 1;
    public bool MoveRight;

    // Start is called before the first frame update
    

    // Update is called once per frame
    void Update(){
        if (MoveRight) {
            transform.Translate(2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector2(0.7f, 0.7f);
        } else {
            transform.Translate(-2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector2(-0.7f, 0.7f);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {        
        if(other.tag == "Turn") {
            Debug.Log("Turn now");
            if (MoveRight) {
                MoveRight = false;
            } else {
                MoveRight = true;
            }
        }
    }
}
