﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManagerScript : MonoBehaviour{

    public static AudioClip coinCollectSound, jumpSound, hitSound, hitSound2;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start(){

        coinCollectSound = Resources.Load<AudioClip>("coin_07");
        jumpSound = Resources.Load<AudioClip>("jump_06");
        hitSound = Resources.Load<AudioClip>("hit_09");
        hitSound2 = Resources.Load<AudioClip>("hit_11");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update(){
        
    }

    public static void PlaySound (string clip) {
        switch (clip) {
            case "coin_07":
                audioSrc.PlayOneShot(coinCollectSound);
                break;
            case "jump_06":
                audioSrc.PlayOneShot(jumpSound);
                break;
            case "hit_09":
                audioSrc.PlayOneShot(hitSound);
                break;
            case "hit_11":
                audioSrc.PlayOneShot(hitSound2);
                break;
        }
    }

}
