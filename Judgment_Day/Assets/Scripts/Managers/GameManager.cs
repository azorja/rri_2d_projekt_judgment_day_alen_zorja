﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public bool btnStart;
    public bool btnQuit;
    public int coins;
    public Text coinText;
    

    public static GameManager sharedInstance = null;

    void Awake() {
        if (sharedInstance != null && sharedInstance != this) {
            Destroy(this.gameObject);
        } else {
            GameObject.DontDestroyOnLoad(gameObject);
            sharedInstance = this;
        }
    }
    // Start is called before the first frame update
    void Start() {
        //SceneManager.sceneLoaded += OnSceneLoaded;
        coinText.text = "Coins: " + coins;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey("escape")) {
            Application.Quit();
        }
    }
    
    public void AddCoins(int numberOfCoins) {
        coins += numberOfCoins;
        coinText.text = "Coins: " + coins;
    }

}
