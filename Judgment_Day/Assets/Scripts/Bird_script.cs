﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird_script : MonoBehaviour{
    public float speed = 1;
    public bool MoveRight;        

    // Update is called once per frame
    void Update() {
        if (MoveRight) {
            transform.Translate(2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector2(0.1f, 0.1f);
        } else {
            transform.Translate(-2 * Time.deltaTime * speed, 0, 0);
            transform.localScale = new Vector2(-0.1f, 0.1f);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Turn") {
            Debug.Log("Turn now");
            if (MoveRight) {
                MoveRight = false;
            } else {
                MoveRight = true;
            }
        }
    }
}
